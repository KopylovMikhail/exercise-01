package ru.kopylov.enumerated;

/**
 * Набор ролей пользователя
 * @see ru.kopylov.domain.User
 */
public enum Role {

    ADMIN("administrator"),
    USER("user");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Возвращает отображаемое имя роли
     */
    public String getDisplayName() {
        return displayName;
    }

}
