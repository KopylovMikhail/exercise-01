package ru.kopylov.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kopylov.dto.PhoneModelDto;
import ru.kopylov.service.PhoneModelService;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1/phone")
public class PhoneModelController {
    
    final private PhoneModelService phoneModelService;

    public PhoneModelController(PhoneModelService phoneModelService) {
        this.phoneModelService = phoneModelService;
    }

    @PostMapping
    public ResponseEntity<PhoneModelDto> savePhoneModel(@RequestBody PhoneModelDto phoneModelDto, UriComponentsBuilder componentsBuilder) {
        PhoneModelDto result = phoneModelService.save(phoneModelDto);
        URI uri = componentsBuilder.path("/api/v1/phone/" + result.getId()).buildAndExpand(result).toUri();
        return ResponseEntity.created(uri).body(result);
    }

    @GetMapping(value = "/{id}")
    public PhoneModelDto getPhoneModelById(@PathVariable String id) {
        return phoneModelService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public PhoneModelDto deletePhoneModelById(@PathVariable String id) {
        return phoneModelService.deleteById(id);
    }

    @PutMapping(value = "/{id}")
    public PhoneModelDto updatePhoneModel(@PathVariable String id, @RequestBody PhoneModelDto phoneModelDto) {
        if (!id.equals(phoneModelDto.getId())) return null;
        return phoneModelService.update(phoneModelDto);
    }

    @GetMapping
    public Collection<PhoneModelDto> getAllPhoneModels(@RequestBody PhoneModelDto phoneModelDto) {
        return phoneModelService.getAll(phoneModelDto);
    }

}
