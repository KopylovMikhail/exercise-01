package ru.kopylov.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kopylov.dto.OrderDto;
import ru.kopylov.service.OrderService;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController {
    
    final private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/user/{userId}")
    public Collection<OrderDto> getOrderByUserId (@PathVariable String userId) {
        return orderService.getByUserId(userId);
    }

    @PostMapping
    public ResponseEntity<OrderDto> saveOrder(@RequestBody OrderDto orderDto, UriComponentsBuilder componentsBuilder) {
        OrderDto result = orderService.save(orderDto);
        URI uri = componentsBuilder.path("/api/v1/order/" + result.getId()).buildAndExpand(result).toUri();
        return ResponseEntity.created(uri).body(result);
    }

    @GetMapping(value = "/{id}")
    public OrderDto getOrderById(@PathVariable String id) {
        return orderService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public OrderDto deleteOrderById(@PathVariable String id) {
        return orderService.deleteById(id);
    }

    @PutMapping(value = "/{id}")
    public OrderDto updateOrder(@PathVariable String id, @RequestBody OrderDto orderDto) {
        if (!id.equals(orderDto.getId())) return null;
        return orderService.update(orderDto);
    }

    @GetMapping
    public Collection<OrderDto> getAllOrders(@RequestBody OrderDto orderDto) {
        return orderService.getAll(orderDto);
    }

}
