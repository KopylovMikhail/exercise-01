package ru.kopylov.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kopylov.dto.BrandDto;
import ru.kopylov.service.BrandService;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1/brand")
public class BrandController {

    final private BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @PostMapping
    public ResponseEntity<BrandDto> saveBrand(@RequestBody BrandDto brandDto, UriComponentsBuilder componentsBuilder) {
        BrandDto result = brandService.save(brandDto);
        URI uri = componentsBuilder.path("/api/v1/brand/" + result.getId()).buildAndExpand(result).toUri();
        return ResponseEntity.created(uri).body(result);
    }

    @GetMapping(value = "/{id}")
    public BrandDto getBrandById(@PathVariable String id) {
        return brandService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public BrandDto deleteBrandById(@PathVariable String id) {
        return brandService.deleteById(id);
    }

    @PutMapping(value = "/{id}")
    public BrandDto updateBrand(@PathVariable String id, @RequestBody BrandDto brandDto) {
        if (!id.equals(brandDto.getId())) return null;
        return brandService.update(brandDto);
    }

    @GetMapping
    public Collection<BrandDto> getAllBrands(@RequestBody BrandDto brandDto) {
        return brandService.getAll(brandDto);
    }

}
