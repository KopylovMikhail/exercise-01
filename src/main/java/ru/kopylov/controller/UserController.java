package ru.kopylov.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kopylov.dto.UserDto;
import ru.kopylov.service.UserService;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    final private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto, UriComponentsBuilder componentsBuilder) {
        UserDto result = userService.save(userDto);
        URI uri = componentsBuilder.path("/api/v1/user/" + result.getId()).buildAndExpand(result).toUri();
        return ResponseEntity.created(uri).body(result);
    }

    @GetMapping(value = "/{id}")
    public UserDto getUserById(@PathVariable String id) {
        return userService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public UserDto deleteUserById(@PathVariable String id) {
        return userService.deleteById(id);
    }

    @PutMapping(value = "/{id}")
    public UserDto updateUser(@PathVariable String id, @RequestBody UserDto userDto) {
        if (!id.equals(userDto.getId())) return null;
        return userService.update(userDto);
    }

    @GetMapping
    public Collection<UserDto> getAllUsers( @RequestBody UserDto userDto) {
        return userService.getAll(userDto);
    }

}
