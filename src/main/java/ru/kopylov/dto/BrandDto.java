package ru.kopylov.dto;

/**
 * DTO-объект для модели бренда Brand
 * @see ru.kopylov.domain.Brand
 */
public class BrandDto {

    /** Идентификатор бренда */
    private String id;

    /** Наименование бренда */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
