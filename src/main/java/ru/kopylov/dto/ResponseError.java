package ru.kopylov.dto;

/**
 * Класс ошибки для передачи в теле REST-запроса
 */
public class ResponseError {

    /** Идентификатор ошибки */
    private String id;

    /** Код ошибки */
    private String code;

    /** Сообщение ошибки */
    private String message;

    /** Наименование системы */
    private String system;

    public ResponseError(String id, String code, String message, String system) {
        this.id = id;
        this.code = code;
        this.message = message;
        this.system = system;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

}
