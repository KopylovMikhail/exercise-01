package ru.kopylov.dto;

import java.math.BigDecimal;

/**
 * DTO-объект для модели заказа Order
 * @see ru.kopylov.domain.Order
 */
public class OrderDto {

    /** Идентификатор заказа */
    private String id;

    /** Итоговая сумма заказа */
    private BigDecimal totalPrice;

    /** Минимальная цена заказа */
    private BigDecimal minPrice;

    /** Максимальная цена заказа */
    private BigDecimal maxPrice;

    /** Имя пользователя, владелеца заказа */
    private String username;

    /** Наименование телефона */
    private String phoneName;

    /** Идентификатор пользователя, владелеца заказа */
    private String userId;

    /** Идентификатор телефона */
    private String phoneId;

    /** Состояние оплаты заказа */
    private boolean paid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

}
