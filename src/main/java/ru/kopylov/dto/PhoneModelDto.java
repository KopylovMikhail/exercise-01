package ru.kopylov.dto;

import ru.kopylov.domain.PhoneModel;

import java.math.BigDecimal;

/**
 * DTO-объект для модели телефона PhoneModel
 * @see PhoneModel
 */
public class PhoneModelDto {

    /** Идентификатор модели телефона */
    private String id;

    /** Наименование модели телефона */
    private String name;

    /** Цена модели телефона */
    private BigDecimal price;

    /** Минимальная цена модели телефона */
    private BigDecimal minPrice;

    /** Максимальная цена модели телефона */
    private BigDecimal maxPrice;

    /** Наименование бренда */
    private String brandName;

    /** Идентификатор бренда */
    private String brandId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

}
