package ru.kopylov.dto;

import ru.kopylov.enumerated.Role;

/**
 * DTO-объект для модели пользователя User
 * @see ru.kopylov.domain.User
 */
public class UserDto {

    /** Идентификатор пользователя */
    private String id;

    /** Логин пользователя */
    private String login;

    /** Пароль пользователя */
    private String password;

    /** Повтор пароля пользователя */
    private String repeatPassword;

    /** Роль пользователя */
    private Role role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
