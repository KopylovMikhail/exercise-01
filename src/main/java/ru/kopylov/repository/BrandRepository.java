package ru.kopylov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.domain.Brand;

/**
 * BrandRepository
 */
@Repository
public interface BrandRepository extends JpaRepository<Brand, String> {

    Brand getById(String id);

    Brand getByName(String name);

}
