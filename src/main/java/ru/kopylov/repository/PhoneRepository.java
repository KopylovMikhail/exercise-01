package ru.kopylov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.domain.PhoneModel;

/**
 * PhoneRepository
 */
@Repository
public interface PhoneRepository extends JpaRepository<PhoneModel, String> {

    PhoneModel getById(String id);

    PhoneModel getByName(String name);

}
