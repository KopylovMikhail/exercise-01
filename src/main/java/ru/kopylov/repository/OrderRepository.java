package ru.kopylov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.domain.Order;

import java.util.List;

/**
 * OrderRepository
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

    List<Order> findByUserId(String userId);

    Order getById(String id);

}
