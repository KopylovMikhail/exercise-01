package ru.kopylov.exception;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.kopylov.dto.ResponseError;

import java.util.UUID;

/**
 * Глобальный обработчик ошибок
 */
@PropertySource("classpath:message.properties")
@RestControllerAdvice(basePackages = "ru.kopylov.controller")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Value("${exHandler.currentSystem}")
    private String currentSystem;

    /**
     * Обрабатывает исключение типа IllegalArgumentException
     * @param exception исключение типа IllegalArgumentException
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ResponseError> illegalArgumentException(IllegalArgumentException exception) {
        ResponseError error = new ResponseError(
                UUID.randomUUID().toString(),
                "illegalArgumentException",
                exception.getMessage(),
                currentSystem
        );
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Обрабатывает исключение типа RuntimeException
     * @param exception исключение типа RuntimeException
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseError> runtimeException(RuntimeException exception) {
        ResponseError error = new ResponseError(
                UUID.randomUUID().toString(),
                "unknown",
                exception.getMessage(),
                currentSystem
        );
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Обрабатывает исключение типа SecurityException
     * @param exception исключение типа SecurityException
     */
    @ExceptionHandler(SecurityException.class)
    public ResponseEntity<ResponseError> securityException(SecurityException exception) {
        ResponseError error = new ResponseError(
                UUID.randomUUID().toString(),
                "securityException",
                exception.getMessage(),
                currentSystem
        );
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Обрабатывает исключение типа NullPointerException
     * @param exception исключение типа NullPointerException
     */
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ResponseError> nullPointerException(NullPointerException exception) {
        ResponseError error = new ResponseError(
                UUID.randomUUID().toString(),
                "securityException",
                exception.getMessage(),
                currentSystem
        );
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
