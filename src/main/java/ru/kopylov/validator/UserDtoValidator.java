package ru.kopylov.validator;

import org.springframework.stereotype.Component;
import ru.kopylov.dto.UserDto;

/**
 * Валидатор соответствия полей пользователя
 */
@Component
public class UserDtoValidator {

    public UserDtoValidator() {
    }

    /**
     * Валидирует на соответствие поля пользователя
     * @param userDto DTO-объект для модели пользователя User
     */
    public void validate(UserDto userDto) {
        int count = 0;
        String message;
        StringBuilder stringBuilder = new StringBuilder();
        if (userDto.getLogin().isEmpty()) {
            stringBuilder.append("Логин не может быть пустым. ");
            count++;
        }
        if (!userDto.getPassword().equals(userDto.getRepeatPassword())) {
            stringBuilder.append("Пароли не совпадают. ");
            count++;
        }
        if (userDto.getPassword().length() < 6) {
            stringBuilder.append("Длина пароля должна быть не меньше 6 символов. ");
        }
        message = stringBuilder.toString();
        if (count > 0) throw new  IllegalArgumentException(message);
    }

}
