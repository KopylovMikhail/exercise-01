package ru.kopylov.service;

import ru.kopylov.dto.BrandDto;

import java.util.Collection;

/**
 * Интерфейс сервиса бренд Brand для работы со слоем репозитория
 * @see ru.kopylov.repository.BrandRepository
 */
public interface BrandService {

    /**
     * Возвращает объект соответствующий записи с указанным полем name
     *
     * @param name наименование бренда
     */
    BrandDto getByName(String name);

    /**
     * Создает новую запись в репозитории, соответствующую объекту Brand
     *
     * @param brandDto dto-объект класса Brand
     */
    BrandDto save(BrandDto brandDto);

    /**
     * Возвращает объект соответствующий записи с указанным id
     *
     * @param id идентификатор бренда
     */
    BrandDto getById(String id);

    /**
     * Удаляет запись об объекте по id
     *
     * @param id идентификатор бренда
     */
    BrandDto deleteById(String id);

    /**
     * Сохраняет состояние объекта
     *
     * @param brandDto dto-объект класса Brand
     */
    BrandDto update(BrandDto brandDto);

    /**
     * Возвращает список объектов соответствующих заданным параметрам
     *
     * @param brandDto dto-объект класса Brand
     */
    Collection<BrandDto> getAll(BrandDto brandDto);

}
