package ru.kopylov.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.dto.UserDto;
import ru.kopylov.enumerated.Role;
import ru.kopylov.domain.User;
import ru.kopylov.repository.UserRepository;
import ru.kopylov.validator.UserDtoValidator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Имплементация UserService
 * @see ru.kopylov.service.UserService
 */
@Service
@Transactional
@PropertySource("classpath:message.properties")
public class UserServiceImpl implements UserService {

    final private UserDtoValidator userDtoValidator;

    final private UserRepository userRepository;

    @Value("${login.exist}")
    private String loginExistMessage;

    public UserServiceImpl(UserDtoValidator userDtoValidator, UserRepository userRepository) {
//        this.userDAO = userDAO;
        this.userDtoValidator = userDtoValidator;
        this.userRepository = userRepository;
    }

    @Override
    public UserDto getByLogin(String login) {
        User user = userRepository.getByLogin(login);
        return modelToDto(user);
    }

    @Override
    public UserDto save(UserDto userDto) {
        userDtoValidator.validate(userDto);
        if (getByLogin(userDto.getLogin()) != null) {
            throw new SecurityException(loginExistMessage);
        }
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        userRepository.save(user);
        return modelToDto(user);
    }

    @Override
    public UserDto getById(String id) {
        User user = userRepository.getById(id);
        return modelToDto(user);
    }

    @Override
    public UserDto deleteById(String id) {
        UserDto userDto = getById(id);
        userRepository.deleteById(id);
        return userDto;
    }

    @Override
    public UserDto update(UserDto userDto) {
        User user = userRepository.getById(userDto.getId());
        user.setRole(userDto.getRole());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        userRepository.save(user);
        return modelToDto(user);
    }

    @Override
    public Collection<UserDto> getAll(UserDto userDto) {
        Collection<User> users = userRepository.findAll();
        List<UserDto> userDtos = new ArrayList<>();
        for (User usr : users) {
            userDtos.add(modelToDto(usr));
        }
        return userDtos;
    }

    private UserDto modelToDto(User user) {
        if (user == null) return null;
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setRole(user.getRole());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setRepeatPassword(user.getPassword());
        return userDto;
    }

}
