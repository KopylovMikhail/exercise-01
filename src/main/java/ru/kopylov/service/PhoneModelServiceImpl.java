package ru.kopylov.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.dto.PhoneModelDto;
import ru.kopylov.domain.Brand;
import ru.kopylov.domain.PhoneModel;
import ru.kopylov.repository.BrandRepository;
import ru.kopylov.repository.PhoneRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Имплементация PhoneModelService
 * @see ru.kopylov.service.PhoneModelService
 */
@Service
@Transactional
public class PhoneModelServiceImpl implements PhoneModelService {

    final private PhoneRepository phoneRepository;

    final private BrandRepository brandRepository;

    public PhoneModelServiceImpl(PhoneRepository phoneRepository, BrandRepository brandRepository) {
        this.phoneRepository = phoneRepository;
        this.brandRepository = brandRepository;
    }

    @Override
    public PhoneModelDto getByName(String name) {
        PhoneModel phoneModel = phoneRepository.getByName(name);
        return modelToDto(phoneModel);
    }

    @Override
    public PhoneModelDto save(PhoneModelDto phoneModelDto) {
        Brand brand = brandRepository.getByName(phoneModelDto.getBrandName());
        PhoneModel phoneModel = new PhoneModel();
        phoneModel.setName(phoneModelDto.getName());
        String price = String.valueOf(phoneModelDto.getPrice());
        phoneModel.setPrice(new BigDecimal(price));
        phoneModel.setBrand(brand);
        phoneRepository.save(phoneModel);
        return modelToDto(phoneModel);
    }

    @Override
    public PhoneModelDto getById(String id) {
        PhoneModel phoneModel = phoneRepository.getById(id);
        return modelToDto(phoneModel);
    }

    @Override
    public PhoneModelDto deleteById(String id) {
        PhoneModelDto phoneModelDto = getById(id);
        phoneRepository.deleteById(id);
        return phoneModelDto;
    }

    @Override
    public PhoneModelDto update(PhoneModelDto phoneModelDto) {
        Brand brand = brandRepository.getByName(phoneModelDto.getBrandName());
        PhoneModel phoneModel = phoneRepository.getById(phoneModelDto.getId());
        phoneModel.setName(phoneModelDto.getName());
        phoneModel.setPrice(phoneModelDto.getPrice());
        phoneModel.setBrand(brand);
        phoneRepository.save(phoneModel);
        return modelToDto(phoneModel);
    }

    @Override
    public Collection<PhoneModelDto> getAll(PhoneModelDto phoneModelDto) {
        Collection<PhoneModel> phoneModels = phoneRepository.findAll();
        List<PhoneModelDto> phoneModelDtos = new ArrayList<>();
        for (PhoneModel phoneModel : phoneModels) {
            phoneModelDtos.add(modelToDto(phoneModel));
        }
        return phoneModelDtos;
    }

    private PhoneModelDto modelToDto(PhoneModel phoneModel) {
        if (phoneModel == null) return null;
        PhoneModelDto phoneModelDto = new PhoneModelDto();
        phoneModelDto.setId(phoneModel.getId());
        phoneModelDto.setName(phoneModel.getName());
        String price = String.valueOf(phoneModel.getPrice());
        phoneModelDto.setPrice(new BigDecimal(price));
        phoneModelDto.setBrandName(phoneModel.getBrand().getName());
        return phoneModelDto;
    }

}
