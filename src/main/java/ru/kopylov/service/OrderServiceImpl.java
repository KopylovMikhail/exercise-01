package ru.kopylov.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.dto.OrderDto;
import ru.kopylov.domain.Order;
import ru.kopylov.domain.PhoneModel;
import ru.kopylov.domain.User;
import ru.kopylov.repository.OrderRepository;
import ru.kopylov.repository.PhoneRepository;
import ru.kopylov.repository.UserRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Имплементация OrderService
 * @see ru.kopylov.service.OrderService
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    final private OrderRepository orderRepository;

    final private UserRepository userRepository;

    final private PhoneRepository phoneRepository;

    public OrderServiceImpl(
            OrderRepository orderRepository,
            UserRepository userRepository,
            PhoneRepository phoneRepository
    ) {

        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.phoneRepository = phoneRepository;
    }

    @Override
    public Collection<OrderDto> getByUserId(String userId) {
        Collection<Order> orders = orderRepository.findByUserId(userId);
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order order : orders) {
            orderDtos.add(modelToDto(order));
        }
        return orderDtos;
    }

    @Override
    public OrderDto save(OrderDto orderDto) {
        User user = userRepository.getByLogin(orderDto.getUsername());
        PhoneModel phoneModel = phoneRepository.getByName(orderDto.getPhoneName());
        Order order = new Order();
        order.setUser(user);
        order.setPhoneModel(phoneModel);
        String price = String.valueOf(orderDto.getTotalPrice());
        order.setTotalPrice(new BigDecimal(price));
        order.setPaid(orderDto.isPaid());
        orderRepository.save(order);
        return modelToDto(order);
    }

    @Override
    public OrderDto getById(String id) {
        Order order = orderRepository.getById(id);
        return modelToDto(order);
    }

    @Override
    public OrderDto deleteById(String id) {
        OrderDto orderDto = getById(id);
        orderRepository.deleteById(id);
        return orderDto;
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        User user = userRepository.getByLogin(orderDto.getUsername());
        PhoneModel phoneModel = phoneRepository.getByName(orderDto.getPhoneName());
        Order order = orderRepository.getById(orderDto.getId());
        order.setPaid(orderDto.isPaid());
        order.setUser(user);
        order.setPhoneModel(phoneModel);
        String price = String.valueOf(orderDto.getTotalPrice());
        order.setTotalPrice(new BigDecimal(price));
        orderRepository.save(order);
        return modelToDto(order);
    }

    @Override
    public Collection<OrderDto> getAll(OrderDto orderDto) {
        Collection<Order> orders = orderRepository.findAll();
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order order : orders) {
            orderDtos.add(modelToDto(order));
        }
        return orderDtos;
    }

    private OrderDto modelToDto(Order order) {
        if (order == null) return null;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setTotalPrice(order.getTotalPrice());
        orderDto.setUsername(order.getUser().getLogin());
        orderDto.setPhoneName(order.getPhoneModel().getName());
        orderDto.setPaid(order.isPaid());
        return orderDto;
    }

}
