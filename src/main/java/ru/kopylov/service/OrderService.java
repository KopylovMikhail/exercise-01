package ru.kopylov.service;

import ru.kopylov.dto.OrderDto;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс сервиса заказа Order для работы со слоем репозитория
 * @see ru.kopylov.repository.OrderRepository
 */
public interface OrderService {

    /**
     * Возвращает список заказов, соответствующий пользователю с указанным id
     *
     * @param userId идентификатор пользователя
     */
    Collection<OrderDto> getByUserId(String userId);

    /**
     * Создает новую запись в репозитории, соответствующую объекту Order
     *
     * @param orderDto dto-объект класса Order
     */
    OrderDto save(OrderDto orderDto);

    /**
     * Возвращает объект соответствующий записи с указанным id
     *
     * @param id идентификатор заказа
     */
    OrderDto getById(String id);

    /**
     * Удаляет запись об объекте по id
     *
     * @param id идентификатор заказа
     */
    OrderDto deleteById(String id);

    /**
     * Сохраняет состояние объекта
     *
     * @param orderDto dto-объект класса Order
     */
    OrderDto update(OrderDto orderDto);

    /**
     * Возвращает список объектов соответствующих заданным параметрам
     *
     * @param orderDto dto-объект класса Order
     */
    Collection<OrderDto> getAll(OrderDto orderDto);

}
