package ru.kopylov.service;

import ru.kopylov.dto.UserDto;

import java.util.Collection;

/**
 * Интерфейс сервиса пользователя User для работы со слоем репозитория
 * @see ru.kopylov.repository.UserRepository
 */
public interface UserService {

    /**
     * Возвращает объект соответствующий записи с указанным полем login
     *
     * @param login логин пользователя
     */
    UserDto getByLogin(String login);

    /**
     * Создает новую запись в репозитории, соответствующую объекту User
     *
     * @param userDto dto-объект класса User
     */
    UserDto save(UserDto userDto);

    /**
     * Возвращает объект соответствующий записи с указанным id
     *
     * @param id идентификатор пользователя
     */
    UserDto getById(String id);

    /**
     * Удаляет запись об объекте по id
     *
     * @param id идентификатор пользователя
     */
    UserDto deleteById(String id);

    /**
     * Сохраняет состояние объекта
     *
     * @param userDto dto-объект класса User
     */
    UserDto update(UserDto userDto);

    /**
     * Возвращает список объектов соответствующих заданным параметрам
     *
     * @param userDto dto-объект класса User
     */
    Collection<UserDto> getAll(UserDto userDto);

}
