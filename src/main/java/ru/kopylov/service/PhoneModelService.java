package ru.kopylov.service;

import ru.kopylov.dto.PhoneModelDto;

import java.util.Collection;

/**
 * Интерфейс сервиса модели телефона PhoneModel для работы со слоем репозитория
 * @see ru.kopylov.repository.PhoneRepository
 */
public interface PhoneModelService {

    /**
     * Возвращает объект соответствующий записи с указанным полем name
     *
     * @param name наименование модели телефона
     */
    PhoneModelDto getByName(String name);

    /**
     * Создает новую запись в репозитории, соответствующую объекту PhoneModel
     *
     * @param phoneModelDto dto-объект класса PhoneModel
     */
    PhoneModelDto save(PhoneModelDto phoneModelDto);

    /**
     * Возвращает объект соответствующий записи с указанным id
     *
     * @param id идентификатор модели телефона
     */
    PhoneModelDto getById(String id);

    /**
     * Удаляет запись об объекте по id
     *
     * @param id идентификатор модели телефона
     */
    PhoneModelDto deleteById(String id);

    /**
     * Сохраняет состояние объекта
     *
     * @param phoneModelDto dto-объект класса PhoneModel
     */
    PhoneModelDto update(PhoneModelDto phoneModelDto);

    /**
     * Возвращает список объектов соответствующих заданным параметрам
     *
     * @param phoneModelDto dto-объект класса PhoneModel
     */
    Collection<PhoneModelDto> getAll(PhoneModelDto phoneModelDto);

}
