package ru.kopylov.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.dto.BrandDto;
import ru.kopylov.domain.Brand;
import ru.kopylov.repository.BrandRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Имплементация BrandService
 * @see ru.kopylov.service.BrandService
 */
@Service
@Transactional
public class BrandServiceImpl implements BrandService {

    final private BrandRepository brandRepository;

    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public BrandDto getByName(String name) {
        Brand brand = brandRepository.getByName(name);
        return modelToDto(brand);
    }

    @Override
    public BrandDto save(BrandDto brandDto) {
        Brand brand = new Brand();
        brand.setName(brandDto.getName());
        brandRepository.save(brand);
        return modelToDto(brand);
    }

    @Override
    public BrandDto getById(String id) {
        Brand brand = brandRepository.getById(id);
        return modelToDto(brand);
    }

    @Override
    public BrandDto deleteById(String id) {
        BrandDto brandDto = getById(id);
        brandRepository.deleteById(id);
        return brandDto;
    }

    @Override
    public BrandDto update(BrandDto brandDto) {
        Brand brand = brandRepository.getById(brandDto.getId());
        brand.setName(brandDto.getName());
        brandRepository.save(brand);
        return modelToDto(brand);
    }

    @Override
    public Collection<BrandDto> getAll(BrandDto brandDto) {
        Collection<Brand> brands = brandRepository.findAll();
        List<BrandDto> brandDtos = new ArrayList<>();
        for (Brand brand : brands) {
            brandDtos.add(modelToDto(brand));
        }
        return brandDtos;
    }

    private BrandDto modelToDto(Brand brand) {
        if (brand == null) return null;
        BrandDto brandDto = new BrandDto();
        brandDto.setId(brand.getId());
        brandDto.setName(brand.getName());
        return brandDto;
    }

}
