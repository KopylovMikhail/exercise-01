package ru.kopylov.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс модели бренда
 */
@Entity
@Table(name = "brands")
public class Brand extends AbstractEntity {

    /** Наименование бренда */
    private String name;

    /** Список всех моделей телефона бренда */
    @OneToMany(mappedBy = "brand", cascade = CascadeType.ALL)
    private List<PhoneModel> phoneModels = new ArrayList<>();

    /**
     * Возвращает наименование бренда
     */
    public String getName() {
        return name;
    }

    /**
     * Устанавливает наименование бренда
     * @param name наименование бренда
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Возвращает список всех моделей телефона бренда
     */
    public List<PhoneModel> getPhoneModels() {
        return phoneModels;
    }

    /**
     * Устанавливает список всех моделей телефона бренда
     * @param phoneModels список всех моделей телефона бренда
     */
    public void setPhoneModels(List<PhoneModel> phoneModels) {
        this.phoneModels = phoneModels;
    }

}
