package ru.kopylov.domain;

import ru.kopylov.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс модели пользователя
 */
@Entity
@Table(name = "users")
public class User extends AbstractEntity {

    /** Логин пользователя */
    private String login;

    /** Пароль пользователя */
    private String password;

    /** Роль пользователя */
    @Enumerated(EnumType.STRING)
    private Role role;

    /** Список заказов пользователя */
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();

    /**
     * Возвращает логин пользователя
     */
    public String getLogin() {
        return login;
    }

    /**
     * Устанавливает логин пользователя
     * @param login логин пользователя
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Возвращает пароль пользователя
     */
    public String getPassword() {
        return password;
    }

    /**
     * Устанавливает пароль пользователя
     * @param password пароль пользователя
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Возвращает роль пользователя
     */
    public Role getRole() {
        return role;
    }

    /**
     * Устанавливает роль пользователя
     * @param role роль пользователя
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Возвращает список заказов пользователя
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Устанавливает список заказов пользователя
     * @param orders список заказов пользователя
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}
