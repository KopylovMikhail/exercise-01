package ru.kopylov.domain;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * Унифицированный абстрактный класс модели объекта
 */
@MappedSuperclass
public abstract class AbstractEntity {

    /** Идентификатор объекта */
    @Id
    private String id = UUID.randomUUID().toString();

    /**
     * Возвращает идентификатор объекта
     */
    public String getId() {
        return id;
    }

    /**
     * Устанавливает идентификатор объекта
     * @param id идентификатор объекта
     */
    public void setId(String id) {
        this.id = id;
    }

}
