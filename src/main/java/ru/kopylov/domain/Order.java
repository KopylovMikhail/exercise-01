package ru.kopylov.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Класс модели заказа
 */
@Entity
@Table(name = "orders")
public class Order extends AbstractEntity {

    /** Итоговая сумма заказа */
    @Column(name = "total_price")
    private BigDecimal totalPrice;

    /** Пользователь, владелец заказа */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /** Телефон в заказе */
    @ManyToOne
    @JoinColumn(name = "phone_id")
    private PhoneModel phoneModel;

    /** Состояние оплаты заказа */
    private boolean paid;

    /**
     * Возвращает итоговую сумма заказа
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * Устанавливает итоговую сумма заказа
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * Возвращает пользователя, владельца заказа
     */
    public User getUser() {
        return user;
    }

    /**
     * Устанавливает пользователя, владельца заказа
     * @param user пользователь, владелец заказа
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Возвращает телефон в заказе
     */
    public PhoneModel getPhoneModel() {
        return phoneModel;
    }

    /**
     * Устанавливает телефон в заказе
     * @param phoneModel телефон в заказе
     */
    public void setPhoneModel(PhoneModel phoneModel) {
        this.phoneModel = phoneModel;
    }

    /**
     * Возвращает состояние оплаты заказа
     */
    public boolean isPaid() {
        return paid;
    }

    /**
     * Устанавливает состояние оплаты заказа
     * @param paid состояние оплаты заказа
     */
    public void setPaid(boolean paid) {
        this.paid = paid;
    }

}
