package ru.kopylov.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс модели телефона
 */
@Entity
@Table(name = "phones")
public class PhoneModel extends AbstractEntity {

    /** Наименование модели телефона */
    private String name;

    /** Цена модели телефона */
    private BigDecimal price;

    /** Наименование бренда */
    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    /** Список заказов телефона */
    @OneToMany(mappedBy = "phoneModel", cascade = CascadeType.ALL)
    List<Order> orders = new ArrayList<>();

    /**
     * Возвращает наименование телефона
     */
    public String getName() {
        return name;
    }

    /**
     * Устанавливает наименование телефона
     * @param name наименование телефона
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Возвращает стоимость телефона
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Устанавливает стоимость телефона
     * @param price стоимость телефона
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Возвращает наименование бренда телефона
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * Устанавливает бренд телефона
     * @param brand бренд телефона
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * Возвращает список заказов телефона
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Устанавливает список заказов телефона
     * @param orders список заказов
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}
