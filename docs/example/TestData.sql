-- заполнение таблицы тестовыми данными
INSERT INTO "users" VALUES ('aa90b5d4-9e91-40bd-b2d6-893c71c68d25', 'admin', 'admin', 'ADMIN'),
                          ('d9b4e6b0-667c-4979-9dd3-ea35cabd0229', 'user', '111111', 'USER'),
                          ('84cb19c7-1f68-428e-bb91-c426009ee5ee', 'user555', '111111', 'USER'),
                          ('8777914d-2458-40ca-b6e3-a68eff24b9aaf', 'customer', '654321', 'USER');
INSERT INTO brands VALUES ('b3a7355c-280d-46c0-94d4-b4a041cca654', 'Xiaomi'),
                         ('5978013b-098a-4185-8d2c-ccc30a890b23', 'Samsung'),
                         ('3ecc317a-e5c1-40dc-8b68-83b92fd8022f', 'Fly');
INSERT INTO phones VALUES ('22174884-b783-49fa-9bf8-5bef19f4fb47', 'Redmi', 10000, 'b3a7355c-280d-46c0-94d4-b4a041cca654'),
                         ('ee0e614c-46d7-42ef-b9b8-2bc207f9987e', 'Poco', 20000, 'b3a7355c-280d-46c0-94d4-b4a041cca654'),
                         ('c97fb8b8-b7a6-401a-9827-de1644c00bbc', 'Galaxy', 25000, '5978013b-098a-4185-8d2c-ccc30a890b23');
INSERT INTO "orders" VALUES ('1048b0c0-cd0e-4566-94f0-daa548b570a1', 10000, 'd9b4e6b0-667c-4979-9dd3-ea35cabd0229',
                            '22174884-b783-49fa-9bf8-5bef19f4fb47', false),
                           ('f3a989de-849d-4a0f-b6a0-8bbc840e6b7f', 20000, 'd9b4e6b0-667c-4979-9dd3-ea35cabd0229',
                            'ee0e614c-46d7-42ef-b9b8-2bc207f9987e', false);