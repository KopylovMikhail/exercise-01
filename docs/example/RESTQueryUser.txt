1) Регистрация нового пользователя
POST: http://localhost:8080/api/v1/user
тело запроса:
{
    "login":"user001",
    "password":"123456",
    "repeatPassword":"123456"
}

2) Получить пользователя по id
GET: http://localhost:8080/api/v1/user/aa90b5d4-9e91-40bd-b2d6-893c71c68d25

3) Удалить пользователя по id
DELETE: http://localhost:8080/api/v1/user/8777914d-2458-40ca-b6e3-a68eff24b9aaf

4) Обновить параметры пользователя
PUT: http://localhost:8080/api/v1/user/aa90b5d4-9e91-40bd-b2d6-893c71c68d25
тело запроса:
{
    "id": "aa90b5d4-9e91-40bd-b2d6-893c71c68d25",
    "login": "administrator",
    "password": "administrator",
    "repeatPassword": "administrator",
    "role": "ADMIN"
}

5) Получить список всех пользователей, имеющих роль USER
GET: http://localhost:8080/api/v1/user
тело запроса:
{
    "role":"USER"
}
